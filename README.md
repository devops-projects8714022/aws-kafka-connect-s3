### Документация для модуля Terraform для развертывания Kafka Connect с использованием Amazon S3

#### Описание
Этот модуль Terraform предназначен для развертывания Kafka Connect с S3 Sink Connector на Amazon Web Services (AWS) с использованием Amazon MSK (Managed Streaming for Apache Kafka) и Amazon MSK Connect. Модуль автоматически создает кластер MSK, конфигурирует MSK Connect и развертывает S3 Sink Connector.

#### Переменные
- `region`: (обязательно) AWS регион, в котором будут развернуты ресурсы.
- `cluster_name`: (обязательно) Имя кластера MSK.
- `number_of_broker_nodes`: (обязательно) Количество брокерных узлов в кластере MSK.
- `subnet_ids`: (обязательно) Список идентификаторов подсетей VPC для кластера MSK.
- `security_group_ids`: (обязательно) Список идентификаторов групп безопасности для кластера MSK.
- `connector_name`: (обязательно) Имя Kafka Connect коннектора.
- `execution_role_arn`: (обязательно) ARN роли IAM для MSK Connect.

#### Выходные значения
- `msk_cluster_arn`: ARN созданного кластера MSK.
- `msk_cluster_bootstrap_brokers`: Bootstrap брокеры для подключения к кластеру MSK.
- `s3_sink_connector_arn`: ARN развернутого S3 Sink Connector.

#### Пример использования

```hcl
module "kafka_connect_s3" {
  source             = "./terraform-kafka-connect-s3"
  region             = "us-west-2"
  cluster_name       = "my-kafka-cluster"
  number_of_broker_nodes = 3
  subnet_ids         = ["subnet-0123456789abcdef0", "subnet-0123456789abcdef1"]
  security_group_ids = ["sg-0123456789abcdef0"]
  connector_name     = "s3-sink-connector"
  execution_role_arn = "arn:aws:iam::123456789012:role/MSKConnectRole"
}
```

### Шаги для использования модуля

1. **Клонирование репозитория модуля**: Клонируйте репозиторий, содержащий данный модуль Terraform.
2. **Конфигурация переменных**: Создайте файл конфигурации Terraform, например `main.tf`, и укажите значения для необходимых переменных, как показано в примере использования.
3. **Инициализация Terraform**: Запустите команду `terraform init` для инициализации конфигурации Terraform.
4. **Применение конфигурации**: Запустите команду `terraform apply` для применения конфигурации и развертывания ресурсов на AWS.
5. **Проверка ресурсов**: После успешного применения конфигурации проверьте созданные ресурсы, такие как кластер MSK и S3 Sink Connector, используя AWS Management Console или AWS CLI.

### Дополнительные замечания
- **Роль IAM**: Убедитесь, что роль IAM, указанная в переменной `execution_role_arn`, имеет необходимые права доступа для взаимодействия с Amazon MSK и S3.
- **Политики S3 Bucket**: Убедитесь, что политики S3 Bucket позволяют доступ из роли IAM.

Этот модуль упрощает процесс развертывания Kafka Connect с S3 Sink Connector на AWS, автоматизируя создание необходимых ресурсов и настройку соединений.