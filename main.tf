provider "aws" {
  region = var.region
}

resource "aws_msk_cluster" "msk_cluster" {
  cluster_name           = var.cluster_name
  kafka_version          = "2.6.0"
  number_of_broker_nodes = var.number_of_broker_nodes

  broker_node_group_info {
    instance_type   = "kafka.m5.large"
    client_subnets  = var.subnet_ids
    security_groups = var.security_group_ids
  }
}

resource "aws_msk_configuration" "msk_configuration" {
  name = "${var.cluster_name}-config"
  kafka_versions = ["2.6.0"]

  server_properties = <<PROPERTIES
auto.create.topics.enable = true
delete.topic.enable = true
PROPERTIES
}

resource "aws_mskconnect_connector" "s3_sink_connector" {
  name                   = var.connector_name
  kafka_cluster {
    apache_kafka_cluster {
      bootstrap_servers = aws_msk_cluster.msk_cluster.bootstrap_brokers_tls
      vpc {
        security_groups = var.security_group_ids
        subnets         = var.subnet_ids
      }
    }
  }
  kafka_connect_version = "2.7.0"
  service_execution_role_arn = var.execution_role_arn
  worker_configuration {
    revision = 1
    worker_configuration_arn = aws_mskconnect_worker_configuration.worker_configuration.arn
  }
  connector_configuration = file("${path.module}/connectors/s3_sink_connector.json")
}

resource "aws_mskconnect_worker_configuration" "worker_configuration" {
  name = "${var.cluster_name}-worker-config"
  properties_file_content = <<PROPERTIES
key.converter=org.apache.kafka.connect.storage.StringConverter
value.converter=org.apache.kafka.connect.storage.StringConverter
tasks.max=1
tasks.retry=3
offset.storage.topic=connect-offsets
config.storage.topic=connect-config
status.storage.topic=connect-status
offset.flush.interval.ms=10000
consumer.override.auto.offset.reset=earliest
PROPERTIES
}

output "msk_cluster_arn" {
  value = aws_msk_cluster.msk_cluster.arn
}

output "msk_cluster_bootstrap_brokers" {
  value = aws_msk_cluster.msk_cluster.bootstrap_brokers_tls
}

output "s3_sink_connector_arn" {
  value = aws_mskconnect_connector.s3_sink_connector.arn
}
