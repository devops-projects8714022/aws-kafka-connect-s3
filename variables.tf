variable "region" {
  description = "The AWS region to deploy resources"
  type        = string
}

variable "cluster_name" {
  description = "The name of the MSK cluster"
  type        = string
}

variable "number_of_broker_nodes" {
  description = "The number of broker nodes in the MSK cluster"
  type        = number
}

variable "subnet_ids" {
  description = "List of VPC subnet IDs for the MSK cluster"
  type        = list(string)
}

variable "security_group_ids" {
  description = "List of security group IDs for the MSK cluster"
  type        = list(string)
}

variable "connector_name" {
  description = "The name of the Kafka Connect connector"
  type        = string
}

variable "execution_role_arn" {
  description = "The ARN of the IAM role for MSK Connect"
  type        = string
}
